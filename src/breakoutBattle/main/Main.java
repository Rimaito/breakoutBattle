package breakoutBattle.main;

import java.awt.Dimension;
import java.util.HashSet;

import javax.swing.JFrame;

import breakoutBattle.Constants;
import breakoutBattle.gui.Window;
import breakoutBattle.gui.game.GameView;
import processing.awt.PSurfaceAWT.SmoothCanvas;
import processing.core.PApplet;

public class Main extends PApplet {
	
	public HashSet<Integer> pressedKeys = new HashSet<>();
	
	public Window window;

	public static void main(String[] args) {
		PApplet.main(Main.class.getName());
	}
	
	@Override
	public void setup() {
		this.background(0);
		this.frameRate(60);
		this.surface.setTitle(Constants.WINDOW_NAME);
		
		//Enable rezising with minimum size to prevent render gliches
		Dimension d = new Dimension(150 ,200);
		SmoothCanvas sc = (SmoothCanvas) this.getSurface().getNative();
		JFrame jf = (JFrame) sc.getFrame();
		jf.setMinimumSize(d);
		this.surface.setResizable(true);
		
		
		window = new Window(this, pressedKeys);
		GameView gameView = new GameView(window);
		window.setView(gameView);
	}
	
	@Override
	public void settings() {
		this.size(720, 1028);
	}
	
	@Override
	public void draw() {
		window.draw();
	}
	
	@Override
	public void exit() {
		super.exit();
	}
	
	@Override
	public void keyPressed() {
		pressedKeys.add(super.keyCode);
	}
	
	@Override
	public void keyReleased() {
		if (pressedKeys.contains(super.keyCode)) pressedKeys.remove(super.keyCode);
	}

}
