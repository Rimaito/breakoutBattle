package breakoutBattle;

import java.util.Properties;

public class Config {
	
	public static Properties properties = new Properties();
	
	static {
		properties.setProperty("ball.startSpeed", "11.4");
		properties.setProperty("ball.acceleration", "0.000125");
		properties.setProperty("ball.size", "30");
		properties.setProperty("padPlayer1.sensitivity", "20");
		properties.setProperty("padPlayer2.sensitivity", "20");
	}

}
