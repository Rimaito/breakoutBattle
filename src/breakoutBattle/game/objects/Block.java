package breakoutBattle.game.objects;

import breakoutBattle.game.Field;
import breakoutBattle.game.HitBox;

public class Block extends GameObject {
	
	private HitBox outerHitBox;
	
	public int hitPoints;
	
	private int sizeX;
	private int sizeY;
	
	public static final int DEFAULT_SIZE_X = 60;
	public static final int DEFAULT_SIZE_Y = 30;

	public Block(Field field, int posX, int posY, int sizeX, int sizeY, int hitPoints) {
		super(field, posX, posY);
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.hitPoints = hitPoints;
		outerHitBox = new HitBox(this, 0, 0, sizeX, sizeY);
	}
	
	public Block(Field field, int posX, int posY, int hitPoints) {
		this(field, posX, posY, DEFAULT_SIZE_X, DEFAULT_SIZE_Y, hitPoints);
	}

	@Override
	public void draw() {
		pApplet.pushStyle();
		if (hitPoints == 1) pApplet.fill(0, 255, 0);
		if (hitPoints == 2) pApplet.fill(255, 255, 0);
		if (hitPoints == 3) pApplet.fill(255, 128, 0);
		if (hitPoints == 4) pApplet.fill(255, 0, 0);
		if (hitPoints == 5) pApplet.fill(255, 0, 255);
		if (hitPoints == 5) pApplet.fill(0, 0, 255);
		if (hitPoints > 5) pApplet.fill(255);
		field.rect(posX, posY, sizeX, sizeY);
		pApplet.popStyle();
	}

	@Override
	public HitBox getOuterHitbox() {
		return outerHitBox;
	}

	@Override
	public void setCollided() {
		if (hitPoints <= 5 && hitPoints > 0) {
			field.scoreListener.blockHit(this);
			hitPoints--;
		}
		if (hitPoints <= 0) {
			field.blocks.remove(this);
		}
	}

	@Override
	public void drawHitBox() {
		outerHitBox.draw();
	}
	
}
