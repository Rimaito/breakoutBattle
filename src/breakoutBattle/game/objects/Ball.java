package breakoutBattle.game.objects;

import java.util.Objects;

import breakoutBattle.Config;
import breakoutBattle.game.Collidable;
import breakoutBattle.game.Field;
import breakoutBattle.game.HitBox;
import breakoutBattle.game.Runnable;
import processing.core.PConstants;

public class Ball extends GameObject implements Runnable {	
	private final HitBox outerHitBox;
	private final HitBox[] hitBoxes = new HitBox[3];
		
	public int sizeX;
	public int sizeY;
	
	public float moveX;
	public float moveY;
	public float speed;

	public Ball(Field field, int posX, int posY, int size) {
		super(field, posX, posY);
		sizeX = size;
		sizeY = size;
		outerHitBox = new HitBox(this, sizeX, sizeY);
		hitBoxes[0] = new HitBox(this, sizeX/4, 0, sizeX/2, sizeY);
		hitBoxes[1] = new HitBox(this, 0, sizeY/4, sizeX, sizeY/2);
		hitBoxes[2] = new HitBox(this, sizeX/8, sizeY/8, (int) (sizeX/8f*6), (int)(sizeY/8f*6));
	}
	
	public void setMove(float moveX, float moveY) {
		float dendminator = Math.abs(moveX) + Math.abs(moveY);
		this.moveX = moveX/dendminator;
		this.moveY = moveY/dendminator;
	}

	@Override
	public void draw() {
		pApplet.pushStyle();
		pApplet.ellipseMode(PConstants.CORNER);
		pApplet.fill(255);
		pApplet.noStroke();
		field.ellipse(posX, posY, sizeX, sizeY);
		pApplet.popStyle();
	}

	
	public int collides(Collidable collidable) {
		if (collidable.equals(this))return 0;
		if (outerHitBox.overlaps(collidable.getOuterHitbox()) != 0) {
			for (int i = 0; i < hitBoxes.length; i++) {
				int collission = hitBoxes[i].overlaps(collidable.getOuterHitbox());
				if (collission != 0) return collission
;			}
		}
		return 0;
	}

	@Override
	public HitBox getOuterHitbox() {
		return outerHitBox;
	}

	@Override
	public void run() {
		for (int i = 0; i < speed; i++) {
			move(moveX, moveY);
			speed += Float.parseFloat(Config.properties.getProperty("ball.acceleration"));
			//Collission with boarders
			if (posX < 0 && moveX < 0) moveX *=  -1;
			if (posX+sizeX > Field.SIZE_X && moveX > 0) moveX *= -1;
			
			if (posY < 0 && moveY < 0) field.end(1);
			if (posY+sizeY > Field.SIZE_Y && moveY > 0) field.end(2);
			
			//collission with everything else
			for (Collidable coll : field.possibleCollissions) {
				int collissionType = collides(coll);
				if (collissionType != 0) {
					coll.setCollided();
					if (collissionType == HitBox.BOTTOM && moveY < 0) moveY *= -1;
					else if (collissionType == HitBox.TOP && moveY > 0) moveY *= -1;
					else if (collissionType == HitBox.LEFT && moveX < 0) moveX *= -1;
					else if (collissionType == HitBox.RIGHT && moveX > 0) moveX *= -1;
				}
			}
		}
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(sizeX, sizeY, posX, posY, moveX, moveY);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Ball) {
			Ball b = (Ball) o;
			if (posX == b.posX && posY == b.posY && sizeX == b.sizeX && sizeY == b.sizeY && moveX == b.moveX && moveY == b.moveY) return true;
			else return false;
		} else return false;
	}

	@Override
	public void drawHitBox() {
		getOuterHitbox().draw();
		for (HitBox hb : hitBoxes) hb.draw();
	}

	@Override
	public void setCollided() {
		
	}

}
