package breakoutBattle.game.objects;

import java.util.Objects;

import breakoutBattle.game.Collidable;
import breakoutBattle.game.Field;
import breakoutBattle.gui.Drawable;
import processing.core.PApplet;

public abstract class GameObject implements Drawable, Collidable {
	
	public final Field field;
	public final PApplet pApplet;
	
	protected float posX;
	protected float posY;
		
	protected GameObject(Field field, int posX, int posY) {
		this.field = field;
		this.pApplet = field.pApplet;
		this.posX = posX;
		this.posY = posY;
	}
	
	public void move(float xMove, float yMove) {
		posX = posX + xMove;
		posY = posY + yMove;
	}
	
	public void setPosition(int posx, int posy) {
		this.posX = posx;
		this.posY = posy;
	}
	
	public float getPosX() {return posX;}
	public float getPosY() {return posY;}
	
	public abstract void drawHitBox();
	
	@Override
	public int hashCode() {
		return Objects.hash(field, posX, posY);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof GameObject) {
			GameObject go = (GameObject) o;
			if (posX == go.posX && posY == go.posY && field.equals(go.field)) return true;
			else return false;
		} else return false;
	}

}
