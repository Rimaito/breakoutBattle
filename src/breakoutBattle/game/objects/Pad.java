package breakoutBattle.game.objects;

import breakoutBattle.Config;
import breakoutBattle.game.Field;
import breakoutBattle.game.HitBox;
import breakoutBattle.game.Runnable;

public class Pad extends GameObject implements Runnable {
	
	public int sizeX = 100;
	public int sizeY = 12;
	
	public float moveX;
	public float speed;
	
	final int player;
	
	public int sensitivityPlayer1;
	public int sensitivityPlayer2;
	
	HitBox outerHitBox = new HitBox(this, sizeX, sizeY);

	public Pad(Field field, int player) {
		super(field, (Field.SIZE_X-100)/2, (player-1)*500 + 150);
		this.player = player;
		sensitivityPlayer1 = Integer.parseInt(Config.properties.getProperty("padPlayer1.sensitivity"));
		sensitivityPlayer2 = Integer.parseInt(Config.properties.getProperty("padPlayer2.sensitivity"));
		if (player == 1) speed = Float.parseFloat(Config.properties.getProperty("padPlayer1.sensitivity"));
		else if (player == 2) speed = Float.parseFloat(Config.properties.getProperty("padPlayer2.sensitivity"));

	}

	@Override
	public void draw() {
		pApplet.pushStyle();
		pApplet.fill(255);
		pApplet.noStroke();
		field.rect(posX, posY, sizeX, sizeY);
		pApplet.popStyle();
	}

	@Override
	public HitBox getOuterHitbox() {
		return outerHitBox;
	}

	@Override
	public void setCollided() {
		float ballX = field.ball.getPosX() + field.ball.sizeX/2;
		ballX -= posX + sizeX/2;
		ballX /= sizeX/2;
		ballX *= 0.48f;
		field.ball.setMove(field.ball.moveX + ballX, field.ball.moveY);
	}

	@Override
	public void drawHitBox() {
		outerHitBox.draw();
	}

	@Override
	public void run() {
		moveX = 0;
		if (player == 2) {
			if (field.gameView.window.pressedKeys.contains(68)) {
				moveX = 1;
			}
			else if (field.gameView.window.pressedKeys.contains(65)) {
				moveX = -1;
			}
		} else if (player == 1) {
			if (field.gameView.window.pressedKeys.contains(39)) {
				moveX = 1;
			}
			else if (field.gameView.window.pressedKeys.contains(37)) {
				moveX = -1;
			}
		}
		
		for (int i = 0; i < speed; i++) {
			//Collission with boarders
			if (posX < 0 && moveX < 0) moveX *=  0;
			if (posX+sizeX > Field.SIZE_X && moveX > 0) moveX *= 0;
			
			//collides with ball
			
			if (field.ball.collides(this) != 0) break;
			move(moveX, 0);
		}
		
	}

	

}
