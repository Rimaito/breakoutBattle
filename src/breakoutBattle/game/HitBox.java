package breakoutBattle.game;

import java.util.Objects;

import breakoutBattle.game.objects.GameObject;
import breakoutBattle.gui.Drawable;

public class HitBox implements Drawable {
	
	public static final int TOP = 1;
	public static final int BOTTOM = 2;
	public static final int LEFT = 3;
	public static final int RIGHT = 4;
	
	private final GameObject gameObject;

	public final int offsetX;
	public final int offsetY;
	
	public final int sizeX;
	public final int sizeY;
	
	public float posX;
	public float posY;
	
	private float top;
	private float bottom;
	private float left;
	private float right;
	
	public HitBox(GameObject gameObject, int offsetX, int offsetY, int sizeX, int sizeY) {
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.gameObject = gameObject;
	}
	
	public HitBox(GameObject gameObject, int sizeX, int sizeY) {
		this(gameObject, 0, 0, sizeX, sizeY);
	}
	
	public int overlaps(HitBox hitBox) {
		updatePos();
		hitBox.updatePos();
		
			if (left < hitBox.right && right > hitBox.left && bottom < hitBox.top && top > hitBox.bottom) {
//				if (right > hitBox.right) return LEFT;
//				if (left < hitBox.left) return RIGHT;
//				if (top > hitBox.top) return BOTTOM;
//				if (bottom < hitBox.bottom) return TOP;
//				else throw new UnexpectedException();
				
				int ovRight = (int) ((right - hitBox.left) *1);
				int ovLeft = (int) ((left - hitBox.right) *-1);
				int ovTop = (int) ((top - hitBox.bottom) *1);
				int ovBottom = (int) ((bottom - hitBox.top) *-1);
				if (ovTop < ovBottom) {
					if (ovRight < ovLeft) {
						if (ovRight < ovTop) return RIGHT;
						else return TOP;
					} else {
						if (ovLeft < ovTop) return LEFT;
						else return TOP;
					}
				} else {
					if (ovRight < ovLeft) {
						if (ovRight < ovBottom) return RIGHT;
						else return BOTTOM;
					} else {
						if (ovLeft < ovBottom) return LEFT;
						else return BOTTOM;
					}
				}
			} else return 0;
		
	}
	
	public void updatePos() {
		posX = gameObject.getPosX() + offsetX;
		posY = gameObject.getPosY() + offsetY;
		
		top = posY + sizeY;
		bottom = posY;
		left = posX;
		right = posX + sizeX;
	}
	
	@Override
	public void draw() {
		updatePos();
		gameObject.pApplet.pushStyle();
		gameObject.pApplet.noFill();
		gameObject.pApplet.stroke(255, 0, 0);
		gameObject.field.rect(this.posX, this.posY, this.sizeX, this.sizeY);
		gameObject.pApplet.popStyle();
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(gameObject, offsetX, offsetY, sizeX, sizeY);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof HitBox) {
			HitBox hb = (HitBox) o;
			if (posX == hb.posX && posY == hb.posY && offsetX == hb.offsetX && offsetY == hb.offsetY && gameObject.equals(hb.gameObject)) return true;
			else return false;
		} else return false;
	}
	
}
