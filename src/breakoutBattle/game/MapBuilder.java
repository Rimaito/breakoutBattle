package breakoutBattle.game;

import java.util.Random;

import breakoutBattle.game.objects.Block;

public class MapBuilder {
	
	public static void setupBuild(Field field) {
		Random rdm = new Random();
		for (int i = 0; i < Field.SIZE_X/Block.DEFAULT_SIZE_X; i++) {
			int hitPoints = rdm.nextInt(3)+3;
			field.blocks.add(new Block(field, Block.DEFAULT_SIZE_X*i, 0, hitPoints));
			field.blocks.add(new Block(field, Field.SIZE_X - Block.DEFAULT_SIZE_X*i - Block.DEFAULT_SIZE_X,Field.SIZE_Y - Block.DEFAULT_SIZE_Y, hitPoints));
		}
		for (int i = 0; i < Field.SIZE_X/Block.DEFAULT_SIZE_X; i++) {
			int hitPoints = rdm.nextInt(3)+1;
			field.blocks.add(new Block(field, Block.DEFAULT_SIZE_X*i, Block.DEFAULT_SIZE_Y, hitPoints));
			field.blocks.add(new Block(field, Field.SIZE_X - Block.DEFAULT_SIZE_X*i - Block.DEFAULT_SIZE_X,Field.SIZE_Y - 2*Block.DEFAULT_SIZE_Y, hitPoints));
		}
		for (int i = 0; i < Field.SIZE_X/Block.DEFAULT_SIZE_X; i++) {
			int hitPoints = rdm.nextInt(5)+1;
			field.blocks.add(new Block(field, Block.DEFAULT_SIZE_X*i, 2*Block.DEFAULT_SIZE_Y, hitPoints));
			field.blocks.add(new Block(field, Field.SIZE_X - Block.DEFAULT_SIZE_X*i - Block.DEFAULT_SIZE_X,Field.SIZE_Y - 3*Block.DEFAULT_SIZE_Y, hitPoints));
		}
		for (int i = 0; i < Field.SIZE_X/Block.DEFAULT_SIZE_X; i++) {
			int hitPoints = rdm.nextInt(2)+3;
			field.blocks.add(new Block(field, Block.DEFAULT_SIZE_X*i, 3*Block.DEFAULT_SIZE_Y, hitPoints));
			field.blocks.add(new Block(field, Field.SIZE_X - Block.DEFAULT_SIZE_X*i - Block.DEFAULT_SIZE_X,Field.SIZE_Y - 4*Block.DEFAULT_SIZE_Y, hitPoints));
		}
	}

}
