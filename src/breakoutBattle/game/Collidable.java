package breakoutBattle.game;

public interface Collidable {
	
	public abstract HitBox getOuterHitbox();
	public abstract void setCollided();
}
