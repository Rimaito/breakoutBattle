package breakoutBattle.game;

import java.util.HashSet;

import breakoutBattle.Config;
import breakoutBattle.game.objects.Ball;
import breakoutBattle.game.objects.Block;
import breakoutBattle.game.objects.Pad;
import breakoutBattle.gui.Drawable;
import breakoutBattle.gui.game.GameView;
import processing.core.PApplet;

public class Field implements Drawable, Runnable {
	
	private long lastRun;
	
	public final GameView gameView;
	public final PApplet pApplet;
	
	private boolean running = false;
	
	public ScoreListener scoreListener = new ScoreListener();
	
	public final HashSet<Collidable> possibleCollissions = new HashSet<>();

	public static final int SIZE_X = 600;
	public static final int SIZE_Y = 800;
	
	private int posX;
	private int posY;
	
	private int sizeX;
	private int sizeY;
	
	private float stretchFactor;
	
	public final HashSet<Block> blocks = new HashSet<>();
	public final HashSet<Runnable> runnableObjects = new HashSet<>();
	public Ball ball;
	public Pad player1;
	public Pad player2;
	
	
	
	public Field(GameView gameView) {
		this.pApplet = gameView.pApplet;
		this.gameView = gameView;
		
		lastRun = System.currentTimeMillis();
	}
	
	public void start() {
		scoreListener.reset();
		blocks.clear();
		runnableObjects.clear();
		ball = new Ball(this, (SIZE_X+30)/2, (SIZE_Y+30)/2, Integer.parseInt(Config.properties.getProperty("ball.size")));
		ball.moveX = 0f;
		ball.moveY = 1f;
		ball.speed = Float.parseFloat(Config.properties.getProperty("ball.startSpeed"));
		player1 = new Pad(this, 1);
		player2 = new Pad(this, 2);
		runnableObjects.add(ball);
		runnableObjects.add(player1);
		runnableObjects.add(player2);
		
		MapBuilder.setupBuild(this);
		
		running = true;
	}
	
	@Override
	public void draw() {
		if (gameView.window.wasRezized()) calcPosition();
		for (Drawable drawabel : blocks) drawabel.draw();
		
		ball.draw();
		ball.drawHitBox();
		
		player1.draw();
		player1.drawHitBox();
		
		player2.draw();
		player2.drawHitBox();
		
		
		//Draw boarders
		pApplet.pushStyle();
		pApplet.noFill();
		pApplet.stroke(255);
		pApplet.rect(posX, posY, sizeX, sizeY);
		pApplet.popStyle();
	}
	

	@Override
	public void run() {
		if (running) {
			possibleCollissions.clear();
			possibleCollissions.addAll(blocks);
			possibleCollissions.add(ball);
			possibleCollissions.add(player1);
			possibleCollissions.add(player2);
			int steps = (int) ((System.currentTimeMillis()-lastRun)/(1000f/60));
			if (steps > 0) lastRun = System.currentTimeMillis();
	
			for (int i = 0; i < steps; i++) {
				for (Runnable runable : runnableObjects) runable.run();
			}
		}
	}
	
	private void calcPosition() {
		
		float sizeX;
		float sizeY;
		
		if (gameView.isLandscape()) {
			sizeY = (float) pApplet.pixelHeight/15*12;
			sizeX = (int) ((double)(sizeY)/SIZE_Y*SIZE_X);
		} else {
			sizeX = (float) pApplet.pixelWidth/15*12;
			sizeY = (int) ((double)(sizeX)/SIZE_X*SIZE_Y);
		}
		
		float posX = pApplet.pixelWidth/2-sizeX/2;
		float posY = pApplet.pixelHeight/2-sizeY/2;
		
		this.posX = (int) posX;
		this.posY = (int) posY;
		this.sizeX = (int) sizeX;
		this.sizeY = (int) sizeY;	
		this.stretchFactor = (float) (sizeX)/SIZE_X;
	}
	
	public void end(int winner) {
		running = false;
		gameView.end(winner);
	}
	
	public float getStreachFactor() {return stretchFactor;}
	
	public float translatePosX(float virtualPosX) {
		return posX + stretchFactor*virtualPosX;
	}
	
	public float translatePosY(float virtualPosY) {
		return posY + stretchFactor*virtualPosY;
	}

	public void rect(float a, float b, float c, float d) {
		pApplet.rect(translatePosX(a), translatePosY(b), c*stretchFactor, d*stretchFactor);
	}

	public void ellipse(float a, float b, float c, float d) {
		pApplet.ellipse(translatePosX(a), translatePosY(b), c*stretchFactor, d*stretchFactor);
	}
	
	public void line(float x1, float y1, float x2, float y2) {
		pApplet.line(translatePosX(x1), translatePosY(y1), translatePosX(x2), translatePosY(y2));
	}
	
}
