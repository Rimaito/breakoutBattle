package breakoutBattle.game;

import breakoutBattle.game.objects.Block;

public class ScoreListener {
	
	public int scorePlayer1 = 0;
	public int scorePlayer2 = 0;
	
	public void blockHit(Block b) {
		int addPoints = b.hitPoints * 10;
		if (b.getPosY() > Field.SIZE_Y/2) scorePlayer2 += addPoints;
		else scorePlayer1 += addPoints;
	}
	
	public void reset() {
		scorePlayer1 = 0;
		scorePlayer2 = 0;
	}

}
