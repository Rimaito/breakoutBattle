package breakoutBattle.gui;

import java.util.HashSet;

import processing.core.PApplet;

public class Window implements Drawable {
	
	public final PApplet pApplet;
	public HashSet<Integer> pressedKeys;
		
	private View activeView;
	
	private int prevPixelWidth;
	private int prevPixelHeight;
	private boolean wasRezized = false;
		
	public Window(PApplet pApplet, HashSet<Integer> pressedKeys) {
		this.pApplet = pApplet;
		this.pressedKeys = pressedKeys;
	}

	@Override
	public void draw() {
		pApplet.background(0);
		if (prevPixelWidth != pApplet.pixelWidth || prevPixelHeight != pApplet.pixelHeight) wasRezized = true;
		activeView.draw();
		prevPixelWidth = pApplet.pixelWidth;
		prevPixelHeight = pApplet.pixelHeight;
		wasRezized = false;
	}
	
	public void setView(View view) {
		if (activeView != null) activeView.close();
		activeView = view;
		activeView.open();
	}
	
	public boolean wasRezized() {return wasRezized;}

}
