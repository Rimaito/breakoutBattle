package breakoutBattle.gui;

import processing.core.PApplet;

public abstract class View implements Drawable {
	
	public final Window window;
	public final PApplet pApplet;
	
	protected View(Window window) {
		this.window = window;
		this.pApplet = window.pApplet;
	}
	
	public abstract void open();
	public abstract void close();

}
