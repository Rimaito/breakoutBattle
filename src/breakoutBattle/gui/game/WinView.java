package breakoutBattle.gui.game;

import breakoutBattle.game.Field;
import breakoutBattle.gui.View;
import processing.core.PConstants;

public class WinView extends View {
	
	final int winner;
	final private GameView gameView;
	private String text;

	protected WinView(GameView gameView, int winner) {
		super(gameView.window);
		this.gameView = gameView;
		this.winner = winner;
		if (winner == 1) text = "Player 1 has won!";
		else if (winner == 2) text = "Player 2 has won!";
		else text = "Draw";
	}

	@Override
	public void draw() {
		pApplet.textSize(gameView.field.getStreachFactor() * 30);
		pApplet.textAlign(PConstants.CENTER);
		pApplet.text(text, gameView.field.translatePosX(Field.SIZE_X/2), gameView.field.translatePosY(Field.SIZE_Y/2)-gameView.field.getStreachFactor() * 15);
	}

	@Override
	public void open() {
		
	}

	@Override
	public void close() {
		
	}

}
