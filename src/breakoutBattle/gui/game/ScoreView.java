package breakoutBattle.gui.game;


import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;

import breakoutBattle.game.Field;
import breakoutBattle.game.ScoreListener;
import breakoutBattle.gui.View;
import processing.core.PConstants;
import processing.core.PFont;

public class ScoreView extends View {
	
	private ScoreListener score;
	public final GameView gameView;

	protected ScoreView(GameView gameView) {
		super(gameView.window);
		this.gameView = gameView;
	}

	@Override
	public void draw() {
//		pApplet.textSize(gameView.field.getStreachFactor()*22);
		pApplet.textSize(gameView.field.getStreachFactor()*26f);
		pApplet.textAlign(PConstants.LEFT);
		pApplet.text("Score Player 2: " + score.scorePlayer2, gameView.field.translatePosX(0), gameView.field.translatePosY(-10));
		pApplet.textAlign(PConstants.RIGHT);
		pApplet.text("Score Player 1: " + score.scorePlayer1, gameView.field.translatePosX(Field.SIZE_X), gameView.field.translatePosY(Field.SIZE_Y + 25));

	}

	@Override
	public void open() {
		score = gameView.field.scoreListener;
		PFont font = null;
		try {
			font = new PFont(Font.createFont(Font.TRUETYPE_FONT, ClassLoader.getSystemResourceAsStream("fonts/Orbitron-Medium.ttf")), false);
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}
		pApplet.textFont(font);
	}

	@Override
	public void close() {
		
	}

}
