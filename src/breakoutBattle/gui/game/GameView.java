package breakoutBattle.gui.game;

import breakoutBattle.game.Field;
import breakoutBattle.gui.View;
import breakoutBattle.gui.Window;

public class GameView extends View {
			
	private boolean landscape;
	
	public Field field;
	public ScoreView scoreView;
	public WinView winView = null;
		
	public GameView(Window window) {
		super(window);
	}

	@Override
	public void draw() {
		if (window.wasRezized()) calcLandscape();
		field.draw();
		scoreView.draw();
		field.run();
		if (winView != null) winView.draw();
	}

	@Override
	public void open() {
		field = new Field(this);
		scoreView = new ScoreView(this);
		scoreView.open();
		field.start();
	}

	@Override
	public void close() {

	}
	
	public void end(int winner) {
		WinView winView = new WinView(this, winner);
		this.winView = winView;
	}
	
	public void calcLandscape() {
		if ((float)pApplet.pixelWidth / pApplet.pixelHeight > (float)Field.SIZE_X/Field.SIZE_Y) landscape = true;
		else landscape = false;
	}
	
	public boolean isLandscape() {return landscape;}

}
