package breakoutBattle.gui;

public interface Drawable {
		
	public abstract void draw();

}
